Проект «Arboretum» - электронная картотека флоры.

Общие сведения:
Дендра́рий территория, отведённая под культивацию в открытом грунте древесных растений (деревьев, кустарников, лиан), размещаемых по систематическим, географическим, экологическим, декоративным и другим признакам. Дендрарии имеют научное, учебное, культурно-просветительское или опытно-производственное назначение.

Инструментарий:
	Java Core, SpringBoot, DateBase MySQL, Android Studio;

Цели:
	создание электронного каталога растений, с возможностью его расширения и дополнения, путем добавления новых данных, в научных и просветительских целях;
