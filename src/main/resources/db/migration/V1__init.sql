-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'plants'
-- 
-- ---

DROP TABLE IF EXISTS `plants`;
		
CREATE TABLE `plants` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL COMMENT 'Название',
  `name_latin` VARCHAR(128) NULL DEFAULT NULL COMMENT 'Латинское название',
  `description` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Описание',
  `country_of_origin_id` INTEGER NULL DEFAULT NULL COMMENT 'Страна происхождения',
  `genera_id` INTEGER NULL DEFAULT NULL COMMENT 'id семейства',
  `URL_of_wiki` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Ссылка на википедию',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'users'
-- 
-- ---

DROP TABLE IF EXISTS `users`;
		
CREATE TABLE `users` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `access_level` ENUM('USER', 'EDITOR', 'ADMIN') NOT NULL DEFAULT 'USER' COMMENT 'Уровень доступа',
  `name` VARCHAR(128) NOT NULL COMMENT 'names',
  `username` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `date_registration` DATE NOT NULL COMMENT 'дата устройства',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'countries'
--
-- ---

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `country_name` VARCHAR(128) NOT NULL COMMENT 'Страна происхождения',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'genera'
--
-- ---

DROP TABLE IF EXISTS `genera`;

CREATE TABLE `genera` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `genera_tree` VARCHAR(255) NOT NULL COMMENT 'Древо класса',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'geoposition'
-- Позиция в дендрарии
-- ---

DROP TABLE IF EXISTS `geoposition`;

CREATE TABLE `geoposition` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `sector_name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Позиция в дендрарии';

-- ---
-- Table 'images'
--
-- ---

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `plants_id` INTEGER NOT NULL,
  `plants_entity_id` INTEGER NULL DEFAULT -1,
  `image` VARCHAR(255) NOT NULL COMMENT 'Ссылка на изображение',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'plants_entity'
--
-- ---

DROP TABLE IF EXISTS `plants_entity`;

CREATE TABLE `plants_entity` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `author_id` INTEGER NOT NULL COMMENT 'id посадившего',
  `plants_id` INTEGER NOT NULL COMMENT 'id типа',
  `sector_id` INTEGER NULL DEFAULT NULL COMMENT 'id расположения в дендрарии',
  `date_bd` DATE NOT NULL COMMENT 'Дата посадки',
  `description` VARCHAR(4096),
  `is_Delete` bit NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys
-- ---

ALTER TABLE `plants` ADD FOREIGN KEY (country_of_origin_id) REFERENCES `countries` (`id`);
ALTER TABLE `plants` ADD FOREIGN KEY (genera_id) REFERENCES `genera` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (plants_id) REFERENCES `plants` (`id`);
ALTER TABLE `images` ADD FOREIGN KEY (plants_entity_id) REFERENCES `plants_entity`(`id`);
ALTER TABLE `plants_entity` ADD FOREIGN KEY (author_id) REFERENCES `users` (`id`);
ALTER TABLE `plants_entity` ADD FOREIGN KEY (plants_id) REFERENCES `plants` (`id`);
ALTER TABLE `plants_entity` ADD FOREIGN KEY (sector_id) REFERENCES `geoposition` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `plants` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `users` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `countries` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `genera` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `geoposition` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `images` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `plants_entity` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `plants` (`id`,`name`,`name_latin`,`description`,`country_of_origin_id`,`genera_id`,`URL_of_wiki`) VALUES
-- ('','','','','','','');
-- INSERT INTO `users` (`id`,`access_level`,`name`,`username`,`password`,`date_registration`) VALUES
-- ('','','','','','');
-- INSERT INTO `countries` (`id`,`country_name`) VALUES
-- ('','');
-- INSERT INTO `genera` (`id`,`genera_tree`) VALUES
-- ('','');
-- INSERT INTO `geoposition` (`id`,`sector_name`) VALUES
-- ('','');
-- INSERT INTO `images` (`id`,`plants_id`,`image`) VALUES
-- ('','','');
-- INSERT INTO `plants_entity` (`id`,`author_id`,`plants_id`,`sector_id`,`photo_id`,`date_bd`,`is_Delete`) VALUES
-- ('','','','','','','');