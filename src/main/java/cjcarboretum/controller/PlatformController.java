package cjcarboretum.controller;

import cjcarboretum.model.Country;
import cjcarboretum.model.Genera;
import cjcarboretum.model.Geoposition;
import cjcarboretum.model.Images;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlatformController {

  // Controller for Country
  @GetMapping("/api/v1/countries")
  public ResponseEntity listCountries() {
    return ResponseEntity.status(HttpStatus.OK).body("getTest Country");
  }

  @GetMapping("/api/v1/countries/{id:\\d+}")
  public ResponseEntity getCountry(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("getWithIdTest Country");
  }

  @PostMapping("/api/v1/countries")
  public ResponseEntity addCountry(Country country) {
    return ResponseEntity.status(HttpStatus.OK).body("postTest Country");
  }

  @DeleteMapping("/api/v1/countries/{id:\\d+}")
  public ResponseEntity deleteCountry(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("deleteTest Country");
  }

  // Controllers for Genera
  @GetMapping("/api/v1/generas")
  public ResponseEntity listGeneras() {
    return ResponseEntity.status(HttpStatus.OK).body("getTest Genera");
  }

  @GetMapping("/api/v1/generas/{id:\\d+}")
  public ResponseEntity getGenera(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("getWithIdTest Genera");
  }

  @PostMapping("/api/v1/generas")
  public ResponseEntity addGenera(Genera genera) {
    return ResponseEntity.status(HttpStatus.OK).body("postTest Genera");
  }

  @DeleteMapping("/api/v1/generas/{id:\\d+}")
  public ResponseEntity deleteGenera(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("deleteTest Genera");
  }

  // Controllers for Geoposition
  @GetMapping("/api/v1/geopositions")
  public ResponseEntity listGeopositions() {
    return ResponseEntity.status(HttpStatus.OK).body("getTest Geoposition");
  }

  @GetMapping("/api/v1/geopositions/{id:\\d+}")
  public ResponseEntity getGeoposition(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("getWithIdTest Geoposition");
  }

  @PostMapping("/api/v1/geopositions")
  public ResponseEntity addGeoposition(Geoposition geoposition) {
    return ResponseEntity.status(HttpStatus.OK).body("postTest Geoposition");
  }

  @DeleteMapping("/api/v1/geopositions/{id:\\d+}")
  public ResponseEntity deleteGeoposition(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("deleteTest Geoposition");
  }

  // Controllers for Images
  @GetMapping("/api/v1/images")
  public ResponseEntity listImages() {
    return ResponseEntity.status(HttpStatus.OK).body("getTest Images");
  }

  @GetMapping("/api/v1/images/{id:\\d+}")
  public ResponseEntity getImages(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("getWithIdTest Images");
  }

  @PostMapping("/api/v1/images")
  public ResponseEntity addImages(Images image) {
    return ResponseEntity.status(HttpStatus.OK).body("postTest Images");
  }

  @DeleteMapping("/api/v1/images/{id:\\d+}")
  public ResponseEntity deleteImages(@PathVariable int id) {
    return ResponseEntity.status(HttpStatus.OK).body("deleteTest Images");
  }
}
