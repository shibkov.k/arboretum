package cjcarboretum;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

  @RequestMapping("/")
  String hello() {
    return "<h1>arboretum</h1>";
  }

}
