package cjcarboretum.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "genera")
public class Genera {

  @Id
  @Column(name = "id")
  private int id;

  @Column(name = "genera_tree")
  private String generaTree;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getGeneraTree() {
    return generaTree;
  }

  public void setGeneraTree(String generaTree) {
    this.generaTree = generaTree;
  }
}
