package cjcarboretum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "images")
public class Images {

  @Id
  @Column(name = "id")
  private int id;

  @Column (name = "plants_id")
  private int plantsId;

  @Column(name = "name")
  private String name;

  @Column(name = "plants_entity_id")
  private int plantsEntityId;

  @Column (name = "image")
  private String image;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getPlantsEntityId() {
    return plantsEntityId;
  }

  public void setPlantsEntityId(int plantsEntityId) {
    this.plantsEntityId = plantsEntityId;
  }

  public int getPlantsId() {
    return plantsId;
  }

  public void setPlantsId(int plantsId) {
    this.plantsId = plantsId;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
