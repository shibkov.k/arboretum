package cjcarboretum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "plants")
public class Plant {

  @Id
  @Column(name = "id")
  private int id;

  @Column(name = "name")
  private String name;

  @Column(name = "name_latin")
  private String nameLatin;

  @Column(name = "description")
  private String description;

  @Column(name = "country_of_origin_id")
  private int countryOfOriginId;

  @Column(name = "genera_id")
  private int generaId;

  @Column(name = "URL_of_wiki")
  private String urlOfWiki;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNameLatin() {
    return nameLatin;
  }

  public void setNameLatin(String nameLatin) {
    this.nameLatin = nameLatin;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getCountryOfOriginId() {
    return countryOfOriginId;
  }

  public void setCountryOfOriginId(int countryOfOriginId) {
    this.countryOfOriginId = countryOfOriginId;
  }

  public int getGeneraId() {
    return generaId;
  }

  public void setGeneraId(int generaId) {
    this.generaId = generaId;
  }

  public String getUrlOfWiki() {
    return urlOfWiki;
  }

  public void setUrlOfWiki(String urlOfWiki) {
    this.urlOfWiki = urlOfWiki;
  }
}
