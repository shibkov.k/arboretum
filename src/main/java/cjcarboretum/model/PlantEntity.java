package cjcarboretum.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "plants_entity")
public class PlantEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private int id;

  @Column(name = "author_id")
  @NotNull
  private int authorId;

  @Column(name = "plants_id")
  @NotNull
  private int plantsId;

  @Column(name = "sector_id")
  @NotNull
  private int sectorId;

  @Column(name = "date_bd")
  @NotNull
  private Date dateBD;

  @Column(name = "is_Delete")
  @NotNull
  private boolean isDelete;

  @Column(name = "name")
  @NotNull
  private String name;

  @Column(name = "description")
  @NotNull
  private String description;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public int getPlantsId() {
    return plantsId;
  }

  public void setPlantsId(int plantsId) {
    this.plantsId = plantsId;
  }

  public int getSectorId() {
    return sectorId;
  }

  public void setSectorId(int sectorId) {
    this.sectorId = sectorId;
  }

  public Date getDateBD() {
    return dateBD;
  }

  public void setDateBD(Date dateBD) {
    this.dateBD = dateBD;
  }

  public boolean isDelete() {
    return isDelete;
  }

  public void setDelete(boolean delete) {
    isDelete = delete;
  }
}
