package cjcarboretum.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
public class User {

  public enum AccessLevel {
    USER, EDITOR, ADMIN
  }

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @NotNull
  private int id;

  @Enumerated(EnumType.STRING)
  @Column(name = "access_level", columnDefinition = "ENUM('USER', 'EDITOR', 'ADMIN')")
  @NotNull
  private AccessLevel userAccess;

  @Column(name = "name")
  @NotNull
  private String name;

  @Column(name = "username")
  @NotNull
  private String username;

  @Column(name = "password")
  @NotNull
  private String password;

  @Column(name = "date_registration")
  @NotNull
  private Date dateRegistration;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public AccessLevel getUserAccess() {
    return userAccess;
  }

  public void setUserAccess(AccessLevel userAccess) {
    this.userAccess = userAccess;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getDateRegistration() {
    return dateRegistration;
  }

  public void setDateRegistration(Date dateRegistration) {
    this.dateRegistration = dateRegistration;
  }
}


